# Docker Getting started

----  ----

## Who am I

- **Dhony Silva**
  - Data Analyst
- **Tiago Rocha**
  - Operations Engineer

----  ----

## Questions that we will try to answer

- What it is docker and why this is important?<br/>

- How to get and install docker?<br/>
 
- What do I need to know to start to use it?<br/>
  
- Can I see it running?<br/>
  
- Where can I learn more about Docker?<br/>

----  ----

## What it is docker and why this is important?

----

<img src="images/homepage-docker-logo.png" alt="Docker Logo" width="300">

Container Plataform to create, share a execute with security any application, in any system.<br/>


----

### What are containers?

A container is a self-contained execution environment that shares the host system kernel and is (optionally) isolated from the other containers on that system.<br/>

----

<!-- .slide: data-background-image="images/container-vm.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Docker's origin

- Announced on 2013 at PyCon, Santa Clara <!-- .element: class="fragment" -->
- Initially based on Linux Container (LXC) <!-- .element: class="fragment" -->
- Released as Open Source in March, 2013 <!-- .element: class="fragment" -->
- By 2014 had already been adopted by Red Hat and Amazon <!-- .element: class="fragment" -->

----

### Docker workflow advantages

- End of: "but it run in my local machine" <!-- .element: class="fragment" -->
- Version control <!-- .element: class="fragment" -->
- Same software artifacts in all environments (dev, test, homologation and production) <!-- .element: class="fragment" -->
- Software abstraction without sacrificing resources <!-- .element: class="fragment" -->

----

### What Docker is not

- Virtualization Platform (VMware, KVM, etc.) <!-- .element: class="fragment" -->
- Cloud Plataform (OpenStack, CloudStack, etc.) <!-- .element: class="fragment" -->
- Configuration management (Puppet, Chef, etc.) <!-- .element: class="fragment" -->
- Deployment Framework (Capistrano, Fabric, etc.) <!-- .element: class="fragment" -->
- Load balance management tool (Mesos, Fleet, etc.) <!-- .element: class="fragment" -->
- Development environment (Vagrant, etc.) <!-- .element: class="fragment" -->

----  ----

## How to get and install docker?

----

### Official site

https://docker.com

----

### Instalation

https://docs.docker.com/install/

----

### Docker on Windows & Docker for Windows

----

<!-- .slide: data-background-image="images/docker-windows-vm-linux.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Docker Desktop for Windows

https://docs.docker.com/docker-for-windows/install/

----

<!-- .slide: data-background-image="images/d4win-artboard4.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Docker Desktop for Mac

https://docs.docker.com/docker-for-mac/install/

----

<!-- .slide: data-background-image="images/d4mac-artboard2.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----  ----

## What do I need to know to start to use it?

----

### Docker Image

----

<!-- .slide: data-background-image="images/docker-layers1.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

<!-- .slide: data-background-image="images/docker-filesystems-busyboxrw.png" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

<img src="images/logo-title-final-registry-2.png" alt="Docker Registry" width="320">

----

### Docker Hub

Docker Container Images library

https://hub.docker.com/

----

### Dockerfile

```docker
FROM alpine:3.8
LABEL maintainer='Tiago Rocha' version='4.4.1'

RUN apk add --quiet --update dhclient && \
    rm -fr /var/cache/apk/*

CMD ["/usr/sbin/dhclient", "-4", "-d"]
```

----

<!-- .slide: data-background-image="images/docker.jpg" data-background-opacity="1.0" data-background-size="contain" data-background-transition="slide" -->

----

### Containers Docker are volatile

----

### Docker Volumes

They are the preferred mechanisms used to data persistence generated and used by Docker containers.


----

### Docker Network

| NOME   | DRIVER | DESCRIÇÃO                                                                |
|:------:|:------:|:------------------------------------------------------------------------ |
| bridge | bridge | The Containers are associated to one specific network.<br/>              |
| host   | host   | Deliver to the container all the existing interfaces on the docker host. |
| none   | null   | Isolate the container for the externals communications.                  |

----

<img src="images/logo-title-final-compose-2b.png" alt="Docker Compose" width="200">

----

### docker-compose

```docker
version: '3'
services:
  php-apache:
    image: php:7.2.1-apache
    ports:
      - 80:80
    volumes:
      - ./DocumentRoot:/var/www/html:z
    links:
      - 'mariadb'

  mariadb:
    image: mariadb:10.1
      volumes:
        - mariadb:/var/lib/mysql
      environment:
        TZ: "Europe/Rome"
        MYSQL_ALLOW_EMPTY_PASSWORD: "no"
        MYSQL_ROOT_PASSWORD: "rootpwd"
        MYSQL_USER: 'testuser'
        MYSQL_PASSWORD: 'testpassword'
        MYSQL_DATABASE: 'testdb'

volumes:
  mariadb:
```

----  ----

## Can I see it running?

----

### Demo!

Everything can fail in this moment! ;p


----  ----

## Where can I learn more about Docker?

----

### Official documentation

https://docs.docker.com/

----

<img src="https://d2sofvawe08yqg.cloudfront.net/dockerparadesenvolvedores/hero?1549480959" alt="Docker Compose" width="200">

### Book Docker for Developers

https://leanpub.com/dockerparadesenvolvedores

https://github.com/gomex/docker-para-desenvolvedores

----

### Docker Telegram group

https://t.me/dockerbr

----

### Play with Docker

https://labs.play-with-docker.com/

----  ----

## Conclusion

Docker is great... <!-- .element: class="fragment" -->


But is not silver bullet! <!-- .element: class="fragment" -->


----  ----

## Thank you!

----  ----

## Contacts

- **Dhony Silva**
  - **LinkedIn:** [linkedin.com/in/dhonysilva](https://www.linkedin.com/in/dhonysilva/)
  - **Blog:** [DevDS](https://devds.wordpress.com)

- **Tiago Rocha**
  - **Blog:** [tiagorocha.eti.br](https://tiagorocha.eti.br/)
  - **Telegram:** [@Tiag0Rocha](https://t.me/Tiag0Rocha)
  - **IRC:** tiagorocha (Freenode e OFTC)
